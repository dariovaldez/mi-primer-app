package com.example.dario.cuartoa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class RecibirParametro extends AppCompatActivity {

    TextView tvRecibirParametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametro);

        tvRecibirParametro = (TextView) findViewById(R.id.tvRecibirParametro);
        Bundle bundle = this.getIntent().getExtras();
        tvRecibirParametro.setText(bundle.getString("dato"));
    }
}
