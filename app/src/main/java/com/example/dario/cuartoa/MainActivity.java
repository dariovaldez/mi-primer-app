package com.example.dario.cuartoa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnLogin, btnRegistro, btnBuscar, btnPasar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin = (Button) findViewById(R.id.btnIngresar);
        btnRegistro = (Button) findViewById(R.id.btnGuardar);
        btnBuscar = (Button) findViewById(R.id.btnBuscar);
        btnPasar = (Button) findViewById(R.id.btnPasar);



        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Login.class);
                startActivity(intent);

            }
        });

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Registrar.class);
                startActivity(intent);

            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Buscar.class);
                startActivity(intent);

            }
        });

        btnPasar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PasarParametro.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.opcionLogin:
                Toast.makeText(MainActivity.this, "Has elegido la opción Login", Toast.LENGTH_SHORT).show();
                break;
            case R.id.opcionRegistrar:
                Toast.makeText(MainActivity.this, "Has elegido la opción de registrar", Toast.LENGTH_SHORT).show();
                break;
            case R.id.opcionLinear:
                Toast.makeText(MainActivity.this, "Has elegido la opción Linear", Toast.LENGTH_SHORT).show();
                break;
            case R.id.opcionFrame:
                Toast.makeText(MainActivity.this, "Has elegido la opción Frame", Toast.LENGTH_SHORT).show();
                break;
            case R.id.opcionTable:
                Toast.makeText(MainActivity.this, "Has elegido la opción Table", Toast.LENGTH_SHORT).show();
                break;
            case R.id.opcionColores:
                Toast.makeText(MainActivity.this, "Has elegido la opción Colores", Toast.LENGTH_SHORT).show();
                break;
            case R.id.opcionAcelerometro:
                Toast.makeText(MainActivity.this, "Has elegido la opción Acelerometro", Toast.LENGTH_SHORT).show();
                break;
            case R.id.opcionProximidad:
                Toast.makeText(MainActivity.this, "Has elegido la opción Proximidad", Toast.LENGTH_SHORT).show();
                break;
            case R.id.opcionLuz:
                Toast.makeText(MainActivity.this, "Has elegido la opción Luz", Toast.LENGTH_SHORT).show();
                break;
            case R.id.opcionVibrar:
                Toast.makeText(MainActivity.this, "Has elegido la opción Vibrar", Toast.LENGTH_SHORT).show();
                break;
            case R.id.opcionLinterna:
                Toast.makeText(MainActivity.this, "Has elegido la opción Linterna", Toast.LENGTH_SHORT).show();
                break;
            case R.id.opcionCamara:
                Toast.makeText(MainActivity.this, "Has elegido la opción Camara", Toast.LENGTH_SHORT).show();
                break;


        }
        return super.onOptionsItemSelected(item);
    }
}
