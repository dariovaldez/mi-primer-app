package com.example.dario.cuartoa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PasarParametro extends AppCompatActivity {

    EditText txtPasarParametro;
    Button btnPasarParametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);

        txtPasarParametro = (EditText) findViewById(R.id.txtPasarParametro);
        btnPasarParametro = (Button) findViewById(R.id.btnPasarParametro);

        btnPasarParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PasarParametro.this, RecibirParametro.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato", txtPasarParametro.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

    }
}
